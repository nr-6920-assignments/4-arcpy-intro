# Intro to ArcPy

This week you get to learn a little bit about using the ArcGIS geoprocessing tools in Python. Yay!

The module for working with geoprocessing tools is called ArcPy, and this week you'll see some of the basics about using it.

See the [homework description](markdown/homework.md).
