import tests.testtools as testtools

__unittest = True

def test_results(problem):
    if problem == 1:
        testtools.Runner.test_output(1, Problem1R)
    elif problem == 2:
        print('This problem has no output to test')
    elif problem == 3:
        testtools.Runner.test_output(3, Problem3R)

def test_notebook(problem):
    if problem == 1:
        testtools.Runner.run_problem(1, [Problem1A, Problem1B])
    elif problem == 2:
        testtools.Runner.run_problem(2, [Problem2])
    elif problem == 3:
        testtools.Runner.run_problem(3, [Problem3])


class Problem1(testtools.BaseTester):
    problem = 1
    suffix = ''
    srs = ''
    success = []
    args = {}

    def setUp(self):
        super().setUp()
        self.args = dict(
            folder = self.data_folder,
            srs = self.srs,
        )

    def test_files(self):
        """Test result files"""
        self.notebook_ran()
        result = self._get_files()
        self.assertGreater(len(result), 0, 'XY not added to any files')
        self.assertEqual(result, self.success, f'XY added to {result} instead of {self.success}')

    def _get_files(self):
        import arcpy
        import tests.testtools.esri as tt_esri
        arcpy.env.workspace = self.data_folder
        fields = set(['POINT_X', 'POINT_Y'])
        files = arcpy.ListFeatureClasses(feature_type='Point')
        return [fn for fn in files if tt_esri.has_fields(fn, fields)]


class Problem1A(Problem1):
    suffix = 'a'
    srs = 'NAD_1983_UTM_Zone_12N'
    success = [u'data1.shp', u'data3.shp', u'data4.shp']


class Problem1B(Problem1):
    suffix = 'b'
    srs = 'GCS_WGS_1984'
    success = [u'data2.shp']


class Problem1R(Problem1A):
    def test_files(self):
        """Test result files"""
        result = self._get_files()
        self.assertGreater(len(result), 0, 'XY not added to any files')
        missing = set(self.success) - set(result)
        self.assertEqual(len(missing), 0, f'XY not added to {", ".join(missing)}')


class Problem2(testtools.BaseTester):
    problem = 2
    args = {}

    def setUp(self):
        super().setUp()
        self.args = dict(
            folder = self.data_folder,
        )


class Problem3(testtools.BaseTester):
    problem = 3
    args = {}

    def setUp(self):
        super().setUp()
        self.args = dict(
            folder = self.data_folder,
            county_file = 'county.shp',
        )

    def test_files(self):
        """Test result files"""
        self.notebook_ran()
        result = self._get_files()
        self.assertEqual(len(result), 4, f'County NAME added to {len(result)} files instead of 4')

    def _get_files(self):
        import arcpy
        import tests.testtools.esri as tt_esri
        arcpy.env.workspace = self.data_folder
        files = arcpy.ListFeatureClasses(feature_type='Point')
        return [fn for fn in files if tt_esri.has_fields(fn, set(['NAME']))]


class Problem3R(Problem3):
    def test_files(self):
        """Test result files"""
        result = self._get_files()
        self.assertGreater(len(result), 0, 'County NAME not added to any files')
        self.assertGreaterEqual(len(result), 4, 'County NAME not added to enough files')


if __name__ == '__main__':
    test_notebook(1)
    test_notebook(2)
    test_notebook(3)
