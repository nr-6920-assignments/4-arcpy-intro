{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Notebook setup\n",
    "\n",
    "**Don't forget to change the path in this cell so that Python can find the datasets for this week.**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Change this to point to your 4-arcpy-intro\\data folder.\n",
    "data_folder = r'D:\\classes\\NR6920\\Assignments\\4-arcpy-intro\\data'"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Import the needed modules.\n",
    "import arcpy\n",
    "import classtools\n",
    "\n",
    "# Set up plotting in the notebook.\n",
    "%matplotlib inline\n",
    "\n",
    "# Set the workspace allow overwrites.\n",
    "arcpy.env.workspace = data_folder\n",
    "arcpy.env.overwriteOutput = True"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Working with environment settings\n",
    "\n",
    "You've seen how to set the `workspace` and `overwriteOutput` environment variables, but you can use any environment settings with ArcPy that you can use with the tools in ArcGIS. For example, say you wanted to use the [Extent setting](https://pro.arcgis.com/en/pro-app/tool-reference/environment-settings/output-extent.htm) to extract a subset of the city boundaries from the cities shapefile. If the extent environment setting is set, and you use a geoprocessing tool that honors it, then the output from the tool will only include the geometries that fall within that extent. \n",
    "\n",
    "Here are the coordinates for the extent we're going to use when trying this out:\n",
    "\n",
    "- Min x: 400000\n",
    "- Max x: 500000\n",
    "- Min y: 4400000\n",
    "- Max y: 4500000\n",
    "\n",
    "The first thing you need to do is create an [Extent](https://pro.arcgis.com/en/pro-app/arcpy/classes/extent.htm) object from those coordinates.\n",
    "\n",
    "```\n",
    "Extent({XMin}, {YMin}, {XMax}, {YMax}, {ZMin}, {ZMax}, {MMin}, {MMax},\n",
    "    {spatial_reference})\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create an extent object.\n",
    "out_extent = arcpy.Extent(400000, 4400000, 500000, 4500000)\n",
    "out_extent"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In order for an environment setting to have an effect, it has to be set on the [env](https://pro.arcgis.com/en/pro-app/arcpy/classes/env.htm) object before you run the geoprocessing tool, so let's do that now. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Set the geoprocessing extent on the environment so that it'll be used.\n",
    "arcpy.env.extent = out_extent"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now any geoprocessing tools that you use during this Python session will only process data that fall in that extent (assuming the tool uses that particular environment variable-- you can look that up in the tool's documentation). For example, from the bottom of the [FeatureClassToFeatureClass](https://pro.arcgis.com/en/pro-app/tool-reference/conversion/feature-class-to-feature-class.htm) documentation, you can see that it honors this environment variable:\n",
    "\n",
    "![feature class to feature class environments](images/fc2fc_env.png)\n",
    "\n",
    "Let's use this tool to extract a subset of the features from cities.shp. Here's the syntax:\n",
    "\n",
    "```\n",
    "arcpy.conversion.FeatureClassToFeatureClass(in_features, out_path, out_name, \n",
    "    {where_clause}, {field_mapping}, {config_keyword})\n",
    "```\n",
    "\n",
    "This tool requires that you specify the output file in two steps. The `out_path` parameter is the folder or geodatabase that you want to put the output in, and the `out_name` parameter is the filename. You'll use the folder that's set in the `arcpy.env.workspace` environment setting for the output path."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Extract the cities from cities.shp that fall within the geoprocessing extent environment variable \n",
    "# set above and save them into cities_subset.shp.\n",
    "result = arcpy.conversion.FeatureClassToFeatureClass(\n",
    "    in_features='cities.shp', \n",
    "    out_path=arcpy.env.workspace, \n",
    "    out_name='cities_subset.shp',\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now plot the results on top of the original."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Use classtools to plot the extracted cities (yellow) on top of the original (blue). \n",
    "# The geoprocessing extent is shown in red.\n",
    "classtools.plot(\n",
    "    data=['cities.shp', 'cities_subset.shp', out_extent], \n",
    "    symbols=['blue', 'yellow', 'red'], \n",
    "    extent='cities_subset.shp',\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The original cities shapefile should be shown in blue and your new subsetted data in yellow. The extent that you used is shown in red. Notice that setting a geoprocessing extent selects everything that overlaps the extent, so the exported polygons aren't necessarily completely contained within the extent of interest and might poke out of the rectangle.\n",
    "\n",
    "Obviously you can turn layers on and off in ArcGIS to make a prettier map if that's what you're using. But I want to see the output in your notebook so that I know it worked!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "*This has nothing to do with ArcPy so you don't need to worry about it unless you want to play with the classtools module, but if you want to plot more than one dataset with the `classtools.plot()` function, you need to pass them all as a list like that last bit of code did. If you don't provide a list of colors, then it will choose colors for you. The `extent` parameter tells it to zoom into the extent of the cities_subset shapefile instead of showing the full extent of all plotted datasets.*"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You constructed the `extent` object yourself for the last subset example, but here's an example that uses the name of the Cache shapefile instead. ArcPy is smart enough to get the extent from the shapefile for you."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Set the geoprocessing extent to the extent of cache.shp.\n",
    "arcpy.env.extent = 'Cache.shp'\n",
    "\n",
    "# Copy cities.shp to cache_subset.shp. Since the geoprocessing\n",
    "# extent is set to Cache County, only those cities will be copied.\n",
    "result = arcpy.conversion.FeatureClassToFeatureClass(\n",
    "    in_features='cities.shp', \n",
    "    out_path=arcpy.env.workspace, \n",
    "    out_name='cache_subset.shp',\n",
    ")\n",
    "\n",
    "# Use classtools to plot the extracted cities (yellow) on top of the original (blue). \n",
    "# The county outline is black and its extent (which is what we used for the \n",
    "# geoprocessing extent) is shown in red.\n",
    "classtools.plot(\n",
    "    data=['Cache.shp', 'cities.shp', 'cache_subset.shp', arcpy.env.extent], \n",
    "    symbols=['white', 'blue', 'yellow', 'red'], \n",
    "    extent='cache_subset.shp',\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "When you plot it this time, the county outline is drawn in black and the red box is the extent of the county polygon. Again, the original shapefile is blue and the subsetted one is yellow. Notice that cities outside of Cache County were selected, because it used the extent (the red box), not the county boundary."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Most geoprocessing tools honor multiple environment settings, and you can set as many of them as you want. For example, you also could've set `arcpy.env.outputCoordinateSystem` to have the data reprojected as it was extracted.\n",
    "\n",
    "You can clear out an environment variable by setting it to `None`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "arcpy.env.extent = None"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now you can use geoprocessing tools without them being affected by this environment setting."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "source": [
    "# Problems\n",
    "\n",
    "Remember when you used [Project](https://pro.arcgis.com/en/pro-app/tool-reference/data-management/project.htm) in the spatial references notebook to reproject the cities shapefile? You're going to reproject using a different method now. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "source": [
    "## Problem 1\n",
    "Set the `outputCoordinateSystem` [environment variable](https://pro.arcgis.com/en/pro-app/arcpy/classes/env.htm) to the spatial reference used by `county_albers.shp` that was created in the last notebook (it might be useful to follow the \"Learn more about outputCoordinateSystem\" link in the documentation)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "source": [
    "## Problem 2\n",
    "\n",
    "Now use [FeatureClassToFeatureClass](https://pro.arcgis.com/en/pro-app/tool-reference/conversion/feature-class-to-feature-class.htm) to copy `county.shp` to another shapefile called `problem1.shp`. If you set the `outputCoordinateSystem` environment variable correctly in problem 1, then the data will automatically be reprojected to Albers when the `FeatureClassToFeatureClass` tool runs."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "source": [
    "Now plot county_albers.shp and problem1.shp. If your plot is yellow, that means that problem1.shp didn't draw on top of county_albers.shp, so something went wrong and it wasn't reprojected to Albers. If the plot is blue, that means that your new shapefile was automatically reprojected and is being drawn on top of the original county_albers shapefile, so you're good.\n",
    "\n",
    "*If you're running this notebook inside ArcGIS and you get a yellow map here but the problem1 shapefile still draws in ArcGIS, that doesn't mean you did it correctly after all. What that means is that ArcGIS is compensating for your mistake and projecting it so it draws correctly on the map-- but the dataset is still wrong!*"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "outputs": [],
   "source": [
    "classtools.plot(\n",
    "    data=['county_albers.shp', 'problem1.shp'], \n",
    "    symbols=['yellow', 'blue'], \n",
    "    extent=arcpy.Extent(-1600000, 1700000, -1240000, 2180000),\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Again, you can set the environment variable you just changed back to `None` so that it doesn't mess with anything else you might do later."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "arcpy.env.outputCoordinateSystem = None"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Make sure you've saved your notebook!**"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.9"
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": false,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {},
   "toc_section_display": true,
   "toc_window_display": true
  },
  "varInspector": {
   "cols": {
    "lenName": 16,
    "lenType": 16,
    "lenVar": 40
   },
   "kernels_config": {
    "python": {
     "delete_cmd_postfix": "",
     "delete_cmd_prefix": "del ",
     "library": "var_list.py",
     "varRefreshCmd": "print(var_dic_list())"
    },
    "r": {
     "delete_cmd_postfix": ") ",
     "delete_cmd_prefix": "rm(",
     "library": "var_list.r",
     "varRefreshCmd": "cat(var_dic_list()) "
    }
   },
   "types_to_exclude": [
    "module",
    "function",
    "builtin_function_or_method",
    "instance",
    "_Feature"
   ],
   "window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
