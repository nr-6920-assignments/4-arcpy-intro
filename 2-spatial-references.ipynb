{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Notebook setup\n",
    "\n",
    "The first code cell of most of the notebooks from here on out will contain a `data_folder` variable that you need to change to match your computer."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "data_folder = r'D:\\classes\\NR6920\\Assignments\\4-arcpy-intro\\data'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The next cell will do some general setup, like import `arcpy`, set `overwriteOutput = True`, and set the workspace to `data_folder` so that it can find the datasets for this week. You saw code like this in the Intro to ArcPy notebook.\n",
    "\n",
    "Sometimes this cell will also import `classtools`, which is a module that I wrote to use with the notebooks and doesn't come with Python or ArcGIS. Instead, it's in your assignment folder. If you've opened the notebook in Jupyter, it can import it just fine from that location, but ArcGIS isn't so smart and requires a few extra steps. These steps won't hurt Jupyter, so I'll include them all the time.\n",
    "\n",
    "`%matplotlib inline` configures the plotting module so that plots will display in the notebook."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Import the needed modules.\n",
    "import arcpy\n",
    "import classtools\n",
    "\n",
    "# Set up plotting in the notebook.\n",
    "%matplotlib inline\n",
    "\n",
    "# Set the workspace allow overwrites.\n",
    "arcpy.env.workspace = data_folder\n",
    "arcpy.env.overwriteOutput = True"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Creating spatial reference objects\n",
    "\n",
    "In the Intro notebook you learned the basics of how to use ArcGIS's geoprocessing tools, but some tools require inputs that aren't simple strings or numbers. Spatial references are one of these data types. If you store a spatial reference object in a variable, you can then ask it things about itself, such as its name. \n",
    "\n",
    "It might help to think of items like strings and numbers as pieces of paper, and objects such as spatial references as filing cabinets that can hold lots of strings and numbers (pieces of paper). You'll work with a lot of data types that are stored as objects-- spatial references are just one of many.\n",
    "\n",
    "[SpatialReference](https://pro.arcgis.com/en/pro-app/arcpy/classes/spatialreference.htm) objects hold information about a spatial reference system (SRS), including the projection and datum specifications. The available data include:\n",
    "\n",
    "- Type of SRS (projected or geographic)\n",
    "- Projected:\n",
    "    - SRS name (e.g. NAD_1983_UTM_Zone_12N)\n",
    "    - Projection name (e.g. Transverse_Mercator)\n",
    "    - Projection parameters, such as false easting, central meridian, and linear units\n",
    "    - Underlying geographic coordinate system\n",
    "- Geographic\n",
    "    - SRS name (e.g. GCS_North_American_1983)\n",
    "    - Geographic coordinate system name (e.g. GCS_North_American_1983)\n",
    "    - Datum name (e.g. D_North_American_1983)\n",
    "    - Datum parameters, such as longitude of prime meridian\n",
    "    - Spheroid name (e.g. GRS_1980)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Loading from a .prj file\n",
    "\n",
    "You're probably familiar with `.prj` files because they're what shapefiles use to store their spatial reference information. Delete a shapefile's `.prj` and it no longer knows what coordinate system it uses.\n",
    "\n",
    "You can load spatial reference information from an existing `.prj` file, but it ignores the `arcpy.env.workspace` setting so you need to provide the full path (don't ask me why-- ask Esri!). Change this so it loads `4-arcpy-intro\\data\\county.prj` on **your** computer."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Load the .prj file into a spatial reference object. DON'T FORGET TO CHANGE THE PATH.\n",
    "srs = arcpy.SpatialReference(r'D:\\classes\\NR6920\\Assignments\\4-arcpy-intro\\data\\county.prj')\n",
    "\n",
    "# Print out the spatial reference name.\n",
    "print(srs.name)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Loading from a geodataset\n",
    "\n",
    "Remember [Describe()](https://pro.arcgis.com/en/pro-app/arcpy/functions/describe.htm) from the Intro notebook? You can use it on a geodataset (shapefile, feature class in a geodatabase, raster, etc.) and then use the [spatialReference](https://pro.arcgis.com/en/pro-app/arcpy/functions/dataset-properties.htm) property on the object loaded by `Describe()`. Unlike using a `.prj` file, arcpy will look in the current workspace when you use `Describe()`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# No need to provide the path this time because Describe uses the\n",
    "# arcpy.env.workspace. Notice that now we're using the .shp file, \n",
    "# not the .prj file.\n",
    "srs = arcpy.Describe('county.shp').spatialReference\n",
    "\n",
    "# Print out the spatial reference name.\n",
    "print(srs.name)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Loading with a name or factory code\n",
    "\n",
    "If you know the name or factory code that Esri uses to identify a particular SRS, then you can use that to create a spatial reference object. \n",
    "\n",
    "Using a name is my least favorite method because figuring out the correct one to use can be difficult. But here's an example for the \"NAD 1983 UTM Zone 12N\" SRS:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create a SRS using the name.\n",
    "srs = arcpy.SpatialReference('NAD 1983 UTM Zone 12N')\n",
    "\n",
    "# Print out the spatial reference name.\n",
    "print(srs.name)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Factory codes are numbers that represent a SRS. and  like a name, each one refers to a particular SRS. The code for the \"NAD 1983 UTM Zone 12N\" SRS is 26912."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create a SRS using the factory code (WKID).\n",
    "srs = arcpy.SpatialReference(26912)\n",
    "\n",
    "# Print out the spatial reference name.\n",
    "print(srs.name)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Fortunately you don't have to guess at what code to use, because Esri publishes lists of spatial reference systems at [projected_coordinate_systems.pdf](https://pro.arcgis.com/en/pro-app/arcpy/classes/pdf/projected_coordinate_systems.pdf) and [geographic_coordinate_systems.pdf](https://pro.arcgis.com/en/pro-app/arcpy/classes/pdf/geographic_coordinate_systems.pdf). The WKID column is the one you're after. Let's create a spatial reference object from the first entry in Table 2 of projected_coordinate_systems.pdf, Abidjan_1987_TM_5_NW."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "srs = arcpy.SpatialReference(2165)\n",
    "print(srs.name)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can also use the name, but you have to remove the underscores. I've run into examples where the name doesn't work, however, so I prefer the numeric code (plus, it's less work because there's less typing and no underscores to remove!)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "arcpy.SpatialReference('Abidjan 1987 TM 5 NW').name"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Getting information from spatial references\n",
    "\n",
    "No matter which method you used to get the spatial reference object, you can still use it the same way. You've seen how to get the name, but let's look at a small subset of the other properties."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create the SRS using the WKID.\n",
    "srs = arcpy.SpatialReference(26912)\n",
    "\n",
    "# Print out some information about it.\n",
    "print('SRS name:', srs.name)\n",
    "print('Projection name:', srs.projectionName)\n",
    "print('Factory code:', srs.factoryCode)\n",
    "print('Type:', srs.type)\n",
    "print('False easting:', srs.falseEasting)\n",
    "print('Linear units:', srs.linearUnitName)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "That was a projected coordinate system, but you can get the underlying geographic coordinate system with the `GCS` property. This returns another spatial reference object, but this one specifies the datum information."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Get the geographic coordinate system (datum) from the SRS.\n",
    "gcs = srs.GCS\n",
    "\n",
    "# Print out some information about it.\n",
    "print('GCS name:', gcs.name)\n",
    "print('Datum name:', gcs.datumName)\n",
    "print('Factory code:', gcs.factoryCode)\n",
    "print('Type:', gcs.type)\n",
    "print('Spheroid name:', gcs.spheroidName)\n",
    "print('Semi-major axis:', gcs.semiMajorAxis)\n",
    "print('Angular units:', gcs.angularUnitName)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "source": [
    "## Problem 1\n",
    "\n",
    "Create a spatial reference object using the factory code (WKID) of 3857. Print out the name of this spatial reference system.\n",
    "\n",
    "*(This is the spatial reference system used by online maps, such as Google Maps.)*"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Using spatial references\n",
    "\n",
    "Once you have a spatial reference object, you can use it as input to geoprocessing tools that want a spatial reference. Let's try using the [Project](https://pro.arcgis.com/en/pro-app/tool-reference/data-management/project.htm) tool to reproject the county shapefile to a geographic coordinate system. Here's the syntax, but follow the [documentation link](https://pro.arcgis.com/en/pro-app/tool-reference/data-management/project.htm) to read about the different parameters.\n",
    "\n",
    "```\n",
    "arcpy.management.Project(in_dataset, out_dataset, out_coor_system, \n",
    "    {transform_method}, {in_coor_system}, {preserve_shape}, {max_deviation}, \n",
    "    {vertical})\n",
    "```\n",
    "\n",
    "All of the examples in this section will reproject county.shp from NAD83 UTM 12N to the USGS Albers projection. In case you're not familiar with this, it's commonly used for datasets that span the 48 contiguous states in the US. It also uses the NAD83 datum, so we won't have to worry about a datum transformation. Its code is 102039.\n",
    "\n",
    "The workspace is already set to the correct folder, so you don't need to use full paths to the shapefiles."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Specify spatial reference with an object\n",
    "\n",
    "Let's use a spatial reference object for the first example, since we've been talking a lot about that. All you need to do is use the factory code to create the spatial reference object, and then pass that object as the `out_coor_system` parameter to the tool."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create a spatial reference object using the USGS Albers WKID.\n",
    "albers = arcpy.SpatialReference(102039)\n",
    "\n",
    "# Reproject county.shp to county_albers.shp using the Albers SRS.\n",
    "result = arcpy.management.Project(\n",
    "    in_dataset='county.shp', \n",
    "    out_dataset='county_albers.shp', \n",
    "    out_coor_system=albers,\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now let's ask the new shapefile about its SRS, just to make sure it worked."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Load the SRS from the new shapefile.\n",
    "srs = arcpy.Describe('county_albers.shp').spatialReference\n",
    "\n",
    "# Print out some info about it.\n",
    "print(srs.type)\n",
    "print(srs.name)\n",
    "print(srs.GCS.name) # datum"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It looks like it did exactly what we wanted."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Specify spatial reference with a WKID\n",
    "\n",
    "You need the spatial reference as an object in order to ask it things about itself, like to get its name or units, but you don't always need the full object for things. For example, many times when using geoprocessing tools you can simplify things by using the WKID or a filename instead of creating an object. \n",
    "\n",
    "This next example uses the WKID of the Albers SRS in order to do the same reprojection as the example above."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "result = arcpy.management.Project(\n",
    "    in_dataset='county.shp', \n",
    "    out_dataset='county_albers2.shp', \n",
    "    out_coor_system=102039,\n",
    ")\n",
    "\n",
    "# Load the SRS from the new shapefile. You can use the result object instead\n",
    "# of the filename if you want.\n",
    "srs = arcpy.Describe(result).spatialReference\n",
    "\n",
    "# Print out some info about it.\n",
    "print(srs.type)\n",
    "print(srs.name)\n",
    "print(srs.GCS.name) # datum"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Specify spatial reference with a filename\n",
    "\n",
    "If you already have a dataset that uses the spatial reference you want, you can use the name of that dataset as the `out_coor_system` parameter. This next example does that and uses the shapefile that you just created (county_albers2.shp) to specify the output spatial reference system."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "result = arcpy.management.Project(\n",
    "    in_dataset='county.shp', \n",
    "    out_dataset='county_albers3.shp', \n",
    "    out_coor_system='county_albers2.shp',\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You could also use a `.prj` file instead of a dataset name, but in that case you need to provide the full path to the file."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Change the path so it works on your computer!\n",
    "result = arcpy.management.Project(\n",
    "    in_dataset='county.shp', \n",
    "    out_dataset='county_albers4.shp',\n",
    "    out_coor_system=r'D:\\classes\\NR6920\\Assignments\\4-arcpy-intro\\data\\county_albers2.prj',\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "source": [
    "## Problem 2\n",
    "\n",
    "Reproject `county_albers2.shp` back to NAD_1983_UTM_Zone_12N. I don't care which method you use or what you call the output file, as long as you don't overwrite the original `county.shp` shapefile. After reprojecting, get and print out the name of the spatial reference from your new file."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Plotting the data\n",
    "\n",
    "This doesn't have anything to do with spatial references specifically, but you can use the classtools module to plot the original and new shapefiles and see how they've changed. You'll see that not only has the shape changed, but the numbers on the axes show that the coordinates have changed. You can't see this in ArcGIS because it reprojects layers on-the-fly when they're add to the map."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Plot the original UTM dataset.\n",
    "classtools.plot('county.shp')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The reprojected one is a different shape and has different numbers along the axes because it uses a different coordinate system."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Plot the Albers dataset.\n",
    "classtools.plot('county_albers.shp')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Spatial reference representations\n",
    "\n",
    "You've seen how Esri represents spatial references, but you may (or may not) be interested to know that there are multiple ways to identify spatial reference systems. One extremely common way is EPSG codes (in fact, many of Esri's factory codes are the same as the EPSG code for that SRS). You can look up spatial references and see the various ways to specify them at http://spatialreference.org/. \n",
    "\n",
    "As an example, check out [EPSG:26912](http://spatialreference.org/ref/epsg/26912/). Want to see an extremely cryptic way of identifying that spatial reference system? Click on the USGS link in the gray box. Conversely, check out the GML format for something extremely long. If you've ever wondered what's in a `.prj` file, follow the ESRI WKT (Well Known Text) link. `.prj` files are actually just text files that you can open in any text editor, and that's what's inside of them.\n",
    "\n",
    "![spatialreference.org](images/epsg.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Make sure you've saved your notebook!**"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.9"
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": false,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {},
   "toc_section_display": true,
   "toc_window_display": true
  },
  "varInspector": {
   "cols": {
    "lenName": 16,
    "lenType": 16,
    "lenVar": 40
   },
   "kernels_config": {
    "python": {
     "delete_cmd_postfix": "",
     "delete_cmd_prefix": "del ",
     "library": "var_list.py",
     "varRefreshCmd": "print(var_dic_list())"
    },
    "r": {
     "delete_cmd_postfix": ") ",
     "delete_cmd_prefix": "rm(",
     "library": "var_list.r",
     "varRefreshCmd": "cat(var_dic_list()) "
    }
   },
   "types_to_exclude": [
    "module",
    "function",
    "builtin_function_or_method",
    "instance",
    "_Feature"
   ],
   "window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
