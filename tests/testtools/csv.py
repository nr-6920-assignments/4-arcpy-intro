import pandas as pd

def compare_csv(master_fn, test_fn, round_fields=None):
    master_df = pd.read_csv(master_fn).rename(columns=str.lower)
    test_df = pd.read_csv(test_fn).rename(columns=str.lower)
    assert set(test_df.columns) == set(master_df.columns), \
        f'Result has columns [{",".join(test_df.columns)}] instead of [{",".join(master_df.columns)}].'
    assert len(test_df) == len(master_df), \
        f'Result has {len(test_df)} rows instead of {len(master_df)}.'
    test_df = test_df[master_df.columns]
    round_fields = round_fields or []
    for field in round_fields:
        master_df[field] = master_df[field].round(3)
        test_df[field] = test_df[field].round(3)
    assert test_df.equals(master_df), 'The result data does not match the expected data.'
