# Create new notebook in Jupyter

To create a new notebook with Jupyter, make sure the main page is open to the assignment folder and then hit the New button in the upper right and choose Python 3-- which should be your only option, unlike mine.

![new notebook](images/new_notebook.png)

## Rename

A new notebook will open up. The first thing you should do is give it a good name. Click on "Untitled" in the upper left corner, and a box will open that lets you rename it. **You don't need to type the .ipynb part.**

![rename notebook](images/rename_notebook.png)

# Create new notebook in ArcGIS

You have three options for creating a new notebook in ArcGIS Pro, all of which will put the notebook in the project folder:

1. Use the New Notebook button on the Insert tab.  
![insert tab](images/arc_insert_nb.png)
2. Use the New button on the Notebook tab.  
![notebook tab](images/arc_new_nb.png)
3. Right-click on Notebooks in the Catalog window.  
![catalog](images/arc_catalog_nb.png)

## Rename

No matter which method you use, you need to rename the notebook so I know what it is. To do that, right-click on the notebook in the Catalog window and choose Rename.

![rename](images/arc_rename_nb.png)

# Working with your new notebook

*These screenshots are from Jupyter, but it's similar in ArcGIS.*

Your new notebook will already have one code cell in it. A new cell will be created when you run that one. You can also add a new cell below the current one with the "insert cell below" button.

![add cell](images/new_cell.png)

You can move the current cell up and down in the notebook with the arrow buttons.

![move cell](images/move_cell.png)

**You need to provide comments for your code.** You can do this either with comment lines that start with `#`, or you can put text in the notebook like I've done in the ones I've given you. To do that, create a new cell and then choose Markdown from the pulldown menu in the toolbar.

![markdown](images/markdown.png)

You can format the text using the syntax described at <https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet>. You can also double-click in any of the text I've written to see how I formatted it. Just run the cell to get the pretty formatting back.
