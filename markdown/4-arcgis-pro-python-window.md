## Using the ArcGIS Pro Python window

There is an interactive Python window in ArcGIS Pro that you can use the same way you're using notebook cells. Let's try it out, but first open ArcGIS Pro and load the cities and Cache shapefiles.

Now open the Python window in ArcGIS Pro by selecting Python from the Analysis tab in the ribbon. Now you can start typing Python code. You don't have to import arcpy, though, because that's automatically done for you.

You'll notice that it will help fill things in for you as you type. The suggestion list gets shorter the more you type and fewer things match what you're entering. For example, the suggestion list started out really long but was narrowed down to only four options by the time I'd typed the third letter of "clip":

![screenshot](images/pro-python-window1.png)

You can use the up and down arrow keys to move through the suggestion list, and either <kbd>Enter</kbd> or <kbd>Tab</kbd> to autocomplete whatever is highlighted in the list. I used <kbd>Tab</kbd> to make my selection and the rest of the name (and the correct capitalization) were filled in for me. Then the list of loaded layers popped up in the selection list and the syntax for the Clip tool was shown below it, with the first parameter in bold:

![screenshot](images/pro-python-window2.png)

I used <kbd>Tab</kbd> to select `"cities"` and as soon as I typed the comma afterwards, the list of layers popped up again, and the second parameter was bold in the syntax box. The syntax box highlights whichever parameter you need to enter in the command.

I selected `"Cache"` for the second parameter and then entered a name for the output file. The process ran after I hit <kbd>Enter</kbd>.

![screenshot](images/pro-python-window3.png)

Take a look at the output filename. I wanted it to go in `D:\classes\WILD6920\Spring2020\python-gis\2-arcpy-intro\data` but it didn't. That's because I hadn't set the workspace or provided a full path. Let's use a full path instead. (You can use the up arrow key to cycle through the last commands you entered and edit them. That can make things much easier!)

Notice the `r` before the path. This would've failed without it.

![screenshot](images/pro-python-window4.png)

Much better! Incidentally, if the cities and Cache shapefiles hadn't been loaded into ArcMap, then I would've had to use full paths or set the workspace in order for ArcMap to find them for the Clip analysis.
