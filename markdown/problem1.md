# Problem 1

If you haven't read through my comments in the [homework](0-homework.md) file, you probably should.

*You might want to make a copy of your `data` folder before doing this problem. That way you can start with a fresh data folder to test things when you think you're done. Otherwise you might think it's working when it's not. People have also been known to accidentally corrupt a file, but that's not a problem if you have a backup copy.*

## Description

Create a notebook called `problem1`. Set up two variables in the very first code cell, with these names and values (except change the path to the data folder on **your** computer:

```
folder = r'D:\classes\NR6920\Assignments\4-arcpy-intro\data'
srs = 'NAD_1983_UTM_Zone_12N'
```

Nothing else should be in that first cell. In the rest of the notebook you need to do these things:

1. Find all point shapefiles in the folder specified by the `folder` variable (see the *Listing data* section of the arcpy-intro notebook).
2. Loop through the shapefiles from step 1. For each one, get the name of its spatial reference. If that name matches what's in the `srs` variable, then: 
    1. Add `POINT_X` and `POINT_Y` fields that contain each point's x,y coordinates to the shapefile's attribute table. *Hint: The appropriate geoprocessing tool is in the [Features toolset](https://pro.arcgis.com/en/pro-app/tool-reference/data-management/an-overview-of-the-features-toolset.htm).*
    2. Print out the name of the modified file and a list of the [fields in the file's attribute table](https://pro.arcgis.com/en/pro-app/arcpy/functions/listfields.htm). If there aren't fields called `POINT_X` and `POINT_Y`, then you didn't actually add the coordinates to the attribute table. See the hint below for code that will get you the list of field names in a shapefile.
3. Don't do anything with the file if the spatial references don't match.
    
## Hints

*This is how you can convert the list of field objects returned by `ListFields()` to a list of field names:*

```python
[field.name for field in arcpy.ListFields(filename)]
```

## Results

There's a description below of what your results should be, but I'm experimenting with letting you run the same tests on your notebooks that I do. These tests can't check for everything, but they can catch a lot. I changed the way they worked in an attempt to make them easier for you to use, so this is still very much an experiment. But open up the `run-tests` notebook and you'll see code to run tests on the results in your `data` folder and also code to run tests on your entire notebooks. If these tests fail then you know that you've done something wrong (unless I've screwed something up with the tests themselves). These tests can't check for output that I've specified that I want to see in your notebook, so you still need to pay attention and include everything I've asked for, even if the tests pass.

These are the files it should find and add coordinates to:

Original data files:

- data1.shp
- data3.shp
- data4.shp

Files created from notebooks:

- random.shp
- sample.shp

My printed output looked like this:

```
data1.shp ['FID', 'Shape', 'Id', 'POINT_X', 'POINT_Y']
data3.shp ['FID', 'Shape', 'Id', 'POINT_X', 'POINT_Y']
data4.shp ['FID', 'Shape', 'Id', 'POINT_X', 'POINT_Y']
random.shp ['FID', 'Shape', 'CID', 'POINT_X', 'POINT_Y']
sample.shp ['FID', 'Shape', 'Id', 'POINT_X', 'POINT_Y']
```

If you look at the attribute table in ArcGIS, it should look something like this:

![attributes](images/hw-p1-attributes.png)

## Clean it up

Before turning it in, make sure there isn't a lot of extra stuff in your notebook for me to wade through and make sure it runs successfully if you restart the kernel (choose Restart & Run All from the Kernel menu). **There should not be any filenames or spatial reference names anywhere in your code except for the first cell with the variables.**
