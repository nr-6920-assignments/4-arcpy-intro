# Problem 3

*Like with problem 1, you might want to make a copy of your `data` folder before doing this in case you accidentally corrupt or overwrite one of the original files.*

## Description

Create a notebook called `problem3`. Set up two variables in the very first code cell, with these names and values (except change the path to the data folder on **your** computer:

```python
folder = r'D:\classes\NR6920\Assignments\4-arcpy-intro\data'
county_file = 'county.shp'
```

You're going to work with the **point** shapefiles in the directory specified by `folder` again for this problem. For each point shapefile, use a [Spatial Join](https://pro.arcgis.com/en/pro-app/latest/tool-reference/analysis/spatial-join.htm) to create a new shapefile with the same points but also information about the county that each point is in. Use the `county_file` variable for the county shapefile.

The Spatial Join tool creates a new feature class instead of adding the column to the existing dataset, so you'll need to create a name for each new file before using the tool. One simple way to do that is add `"_county"` onto the end of each filename. For example, `file1.shp` would become `file1_county.shp`. There are multiple ways to create this name, but an easy one is to use the Python string [`replace()`](https://docs.python.org/3/library/stdtypes.html#str.replace) method to replace `".shp"` with `"_county.shp"`.

**Like problem 1, have it print out the name of each file that it runs the tool on** so that you can see which ones it's processing. That might seem pointless to you, but it's actually really common practice to do something like that. It helps you keep an eye on what your code is doing, especially if it's working on lots of files or for long timespans.

When it finishes adding the county to all of the point shapefiles, confirm to yourself (and prove to me) that one of the new files (like `data1_county.shp`) has the county `NAME` column by displaying the first few rows of the attribute table with this code:

```python
import classtools
classtools.table2pd('data1_county.shp').head() # Use a filename for one of your new files
```

## Hints

Although you could check all of your output files, you only need to do this on one in order to make me happy, because you did the processing in a loop and treated all of the files the same way, right? *Right?* 

So you can assume that if it did it correctly on one, then it did it correctly on all of them. If I'm processing hundreds or thousands of files, I can promise you that I don't open them all up to check the output. Instead, I spot check one (or maybe more if there are different things that could happen) and if those are good, I assume the others are, too, until proven otherwise. (Unfortunately it's not that uncommon that things are proven otherwise-- with real world messy data, there are almost always situations you didn't anticipate that cause problems. But I'm still not going to check 100+ files!) 

## Results

Also, see the `run-tests` notebook. 

These are the filenames it should print out, and it should create a new `_county` file for each of them:

```
data1.shp
data2.shp
data3.shp
data4.shp
random.shp
sample.shp
```

This is a screenshot of part of my `data1_county` shapefile. Notice it has the `NAME` column, but the original `data1.shp` does not.

![attribute table](images/hw-p3-attributes.png)

## Clean it up

Before turning it in, make sure there isn't a lot of extra stuff in your notebook for me to wade through and make sure it runs successfully if you restart the kernel. **There should not be any filenames anywhere in your code except for (1) the first cell with the variables, and (2) the cell that prints out the attribute table for one file using the code I gave you.**
