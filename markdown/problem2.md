# Problem 2

## Description

Say you need a summary of the shapefiles in a folder, including their name, geometry type and spatial reference. Sure, this is easy to do manually if you only have a couple of files, but what if there are 100 of them? I don't know about you, but the thought of having to manually look up that information on 100 different files is enough to give me nightmares.

There aren't anywhere near 100 files in your data folder, but you're still going to write some code to compile and display this information.

Create a notebook called `problem2`. In the very first code cell, set up a single variable called `folder` and store the path to your `4-arcpy-intro\data` folder in it. Nothing else should be in this first cell.

```python
folder = r'D:\classes\NR6920\Assignments\4-arcpy-intro\data'
```

In the rest of the notebook, write code that looks in the directory specified by the `folder` variable and prints out a nice little summary of the GIS datasets in it. Do it in table format, since tables are easy to read. This is what I want it to look like (make sure you read the hints for help formatting it):

```
FILENAME                           GEOMETRY       SPATIAL REFERENCE

Cache.shp                          Polygon        NAD_1983_UTM_Zone_12N
cache_cities.shp                   Polygon        NAD_1983_UTM_Zone_12N
cache_subset.shp                   Polygon        NAD_1983_UTM_Zone_12N
cities.shp                         Polygon        NAD_1983_UTM_Zone_12N
cities_subset.shp                  Polygon        NAD_1983_UTM_Zone_12N
county.shp                         Polygon        NAD_1983_UTM_Zone_12N
county2.shp                        Polygon        NAD_1983_UTM_Zone_12N
county_albers.shp                  Polygon        USA_Contiguous_Albers_Equal_Area_Conic_USGS_version
county_albers2.shp                 Polygon        USA_Contiguous_Albers_Equal_Area_Conic_USGS_version
county_albers3.shp                 Polygon        USA_Contiguous_Albers_Equal_Area_Conic_USGS_version
county_albers4.shp                 Polygon        USA_Contiguous_Albers_Equal_Area_Conic_USGS_version
data1.shp                          Point          NAD_1983_UTM_Zone_12N
data2.shp                          Point          GCS_WGS_1984
data3.shp                          Point          NAD_1983_UTM_Zone_12N
data4.shp                          Point          NAD_1983_UTM_Zone_12N
data5.shp                          Polygon        NAD_1983_UTM_Zone_12N
problem1.shp                       Polygon        USA_Contiguous_Albers_Equal_Area_Conic_USGS_version
random.shp                         Point          NAD_1983_UTM_Zone_12N
sample.shp                         Point          NAD_1983_UTM_Zone_12N
```

## Hints

Here's a nifty little trick with the string `format()` function. Say you want to print out some data in columns and your first try looks like this:

```python
print('{0} {1}'.format('STATE', 'CAPITAL'))
print('{0} {1}'.format('Washington', 'Olympia'))
print('{0} {1}'.format('Oregon', 'Salem'))
```

which prints this:

```
STATE CAPITAL
Washington Olympia
Oregon Salem
```

Those aren't nice columns. There are [**tons** of options](https://docs.python.org/3/library/string.html#formatspec) for customizing how those `{}` placeholders get filled in, and one of them is a width option that will help with this. If you tell it to use a width of 50 but then give it value that's only 35 characters long, it'll fill those remaining 15 spaces with whitespace (by default, but you can use something instead of blank spaces if you want). To specify a width, add a colon after the number inside the braces, and then put a width after the colon. Like this:

```python
print('{0:20}{1}'.format('STATE', 'CAPITAL'))
print('{0:20}{1}'.format('Washington', 'Olympia'))
print('{0:20}{1}'.format('Oregon', 'Salem'))
```

```
STATE               CAPITAL
Washington          Olympia
Oregon              Salem
```

Pretty cool, huh.

It also works with f-strings, but that example works better with variables.

```python
state = 'Utah'
city = 'Salt Lake City'
print(f'{state:20}{city}')
```

```
Utah                Salt Lake City
```

You can test that your notebook runs on restart using code in the `run-tests` notebook. Or you can just restart the kernel and run your notebook again.

## Clean it up

Before turning it in, make sure there isn't a lot of extra stuff in your notebook for me to wade through and make sure it runs successfully if you restart the kernel (choose Restart & Run All from the Kernel menu). **There should not be any filenames anywhere in your code except for the first cell with the folder variable.**
