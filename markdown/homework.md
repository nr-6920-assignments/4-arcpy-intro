# Intro to ArcPy homework

*Due Tuesday, February 16, at midnight (since Monday is a university holiday)*

This week you get to learn a little bit about using the ArcGIS geoprocessing tools in Python. Yay!

## Notebook comprehension questions

*Worth 13 points*

You have your choice if you want to use Jupyter or ArcGIS to read this week's notebooks. If you want to use ArcGIS, make sure you've updated to version 2.7 because it improves notebook handling a lot and I'm going to get rid of all the extra code I had to put in the notebooks last semester to make it work.

If you want to use Jupyter, open the notebooks the same way you always have. If you want to use ArcGIS, open the `4-arcpy-intro.aprx` project file. The notebooks will show up in the Notebooks folder in the Catalog pane, but they're also already open in the project.

![catalog pane](images/arc_catalog.png)

Work through the notebooks in this order:

- 1-arcpy-intro.ipynb (7 points)
- 2-spatial-references.ipynb (3 points)
- 3-environment-settings.ipynb (3 points)
- 4-arcgis-pro-python-window.html - Yeah, this isn't a notebooks and you don't have to turn it in.

## Script problems

*Worth 10 points each*

Create a separate notebook for each problem. There are directions for creating and working with new notebooks in the [create-notebook](create-notebook.md) file.

For each of these problems, I'll give you enough information to tell if you're getting the correct results or not.

> ### Important note for filenames, variables, and code cells!
> 
> Please name your notebooks with the names I give you (with no spaces in the names). Also, I'll tell you variables to set up at the beginning of your notebook. Put these in the **first code cell** in your notebook. **Do not** put anything else in this cell. 
>
> Why the picky rules? Because I'm going to run your notebooks to make sure that they do actually run and get the correct results with different datasets, but there is no way I'm going to manually run about 50 notebooks a couple of times each. Instead, I'll have a script that modifies the values of the variables and automatically runs the notebook. But in order for this to work, you absolutely must put the variables in the first code cell all alone. It already takes me many hours to grade each assignment, and I'll get grumpy and probably take off points if you make it even harder for me by not doing this.

### Notebook layout

You might find it useful to copy the problem description into a markdown cell at the top of the notebook. You can put most of your code in one code cell, but it's nice to be able to break it up into parts. You generally want to import any modules you need in the first code cell of the notebook (although it'll be the second for you, since you're going to put the variables in the first one). I usually set variables and do other setup like changing directories in the second code cell (third for you). Then I break things up in what seems like a logical fashion, or by what code needs to be run together, or by whatever other criteria makes sense to me based on what I'm doing. That way I can intersperse markdown cells to explain my thought processes or why I did something the way I did. The notebooks I set up for you in earlier assignments followed this pattern, except that they put the variables in the second code cell instead of the first.

You will probably create a lot of code cells and write code that isn't needed for the final solution. Please delete all of this extra stuff. You can delete the current cell with the option under the Edit menu. **You wouldn't turn in a rough draft of a paper, so don't turn in a rough draft of your code.**

### Problems

The problem descriptions are long because I included a lot of details, hints, and descriptions of what your results should be. This doesn't mean that the code will be long-- in fact, the code is pretty short for all of these. For each one you'll need to set up a loop, but you can solve the problem with only two or three lines of code inside each loop. 

Pay close attention to the hints I give you-- sometimes students ignore those and then end up wasting hours of their time trying to figure something out that's right there in the problem description. This advice applies to the entire semester! 

Also, every technique you need for the problems is in the notebooks I gave you, although you *will* need to use the Esri documentation to see how to use the geoprocessing tools.

1. Described in the [problem1](problem1.md) file
1. Described in the [problem2](problem2.md) file
1. Described in the [problem3](problem3.md) file

You'll turn these in the same way you turn your other notebooks in. The only difference will be that they'll have a green icon next to their names in GitHub Desktop instead of a yellow one. The green means it's a new file, and yellow means it's modified.


